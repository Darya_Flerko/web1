/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.gsu.epamlab.actions;

/**
 *
 * @author Админ
 */
public class Worker {
    private String FIO;
    private String Dolsh;
    int kab;
    // int id_dolshn;

    public Worker(String FIO, String Dolsh, int kab) {
        this.FIO = FIO;
        this.Dolsh = Dolsh;
        this.kab = kab;
        // this.id_dolshn = id_dolshn;
    }

    public Worker(){}

    public String getFIO() {
        return FIO;
    }

    public void setFIO(String FIO) {
        this.FIO = FIO;
    }

    public String getDolsh() {
        return Dolsh;
    }

    public void setDolsh(String Dolsh) {
        this.Dolsh = Dolsh;
    }

    public int getKab() {
        return kab;
    }

    public void setKab(int kab) {
        this.kab = kab;
    }



    @Override
    public String toString() {
        return "Worker{" + "FIO=" + FIO + ", Dolsh=" + Dolsh + ", kab=" + kab +  '}';
    }


}
