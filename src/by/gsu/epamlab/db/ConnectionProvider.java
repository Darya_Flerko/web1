package by.gsu.epamlab.db;

import javax.naming.InitialContext;
import java.sql.*;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;

/**
 * Created by Админ on 21.03.2017.
 */
public class ConnectionProvider {

    ConnectionProvider() {

    }

    public static Connection getConnection() {
        int k = 3;

        InitialContext initialContext = null;
        Connection con = null;
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=WebHospital;integratedSecurity=true");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (final SQLException e) {
            e.printStackTrace();
        }
        return con;
    }
}
