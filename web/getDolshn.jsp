<%--
  Created by IntelliJ IDEA.
  User: Админ
  Date: 21.03.2017
  Time: 22:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@page import=""%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ page import="java.io.IOException" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="by.gsu.epamlab.actions.DolshnView" %>
<%@ page import="com.microsoft.sqlserver.jdbc.*" %>

<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>

<html>
<head>
    <title>Должности</title>
</head>
<body>
<%
    InitialContext initialContext = null;
    Connection con = null;

    try {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        con = DriverManager.getConnection("jdbc:sqlserver://localhost:1433;databaseName=WebHospital;integratedSecurity=true");


        // Query for get all users
        final PreparedStatement preparedStatement = con.prepareStatement("SELECT * FROM Должности");
        final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            response.getWriter()
                    .println(DolshnView.getHTMLViewOfDolshn(resultSet.getString("name")));
        }
    } catch (final Exception e) {
        response.getWriter().println(e.getMessage());
    } finally {
        try {
            initialContext.close();
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
%>
</body>
</html>
